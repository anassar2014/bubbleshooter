package eecs40.assignment_1;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Ahmed on 3/24/2015.
 */
public class BubbleShooterThread extends Thread {
    BubbleShooterView bsv;

    public BubbleShooterThread(BubbleShooterView bsv) {
        this.bsv= bsv;
    }

    public void run() {
        SurfaceHolder sh = bsv.getHolder();

        while( !Thread.interrupted() ) {
            Canvas c = sh.lockCanvas(null);
            try {
                synchronized(sh) {
                    bsv.renderGame(c);
                }
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                System.err.print( e.getStackTrace() );
            } finally {
                if (c!=null) {
                    sh.unlockCanvasAndPost(c);
                }
            }
            // Set the frame rate by setting this delay
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                // This means that this thread was interrupted while sleeping.
                return;
            }
        }
    }
}
