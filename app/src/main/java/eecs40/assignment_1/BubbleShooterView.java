package eecs40.assignment_1;

/**
 * Created by Ahmed on 3/24/2015.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


///////////////////////////////////////////////////////////////////////////////////////////////
// The SurfaceView is a special subclass of View that offers a dedicated drawing surface within the
// View hierarchy.
///////////////////////////////////////////////////////////////////////////////////////////////
// SurfaceHolder.Callback is an interface that will notify you with information about the
// underlying Surface, such as when it is created, changed, or destroyed. These events are
// important so that you know when you can start drawing, whether you need to make adjustments
// based on new surface properties, and when to stop drawing and potentially kill some tasks.
///////////////////////////////////////////////////////////////////////////////////////////////
// A Drawable is a general abstraction for "something that can be drawn."
///////////////////////////////////////////////////////////////////////////////////////////////
// Property Animation
// https://developer.android.com/guide/topics/graphics/prop-animation.html

public class BubbleShooterView extends SurfaceView implements SurfaceHolder.Callback {
    private static final int VELOCITY_SCALE = 40;
    private static final int DELTA_ALPHA    = 20;
    private static final int minDelta = 20;
    private static final int maxDelta = 50;
    private Random rng;
    private BubbleShooterThread bst;

    private static final int[] bubbleColors = {
            Color.BLACK,
            Color.BLUE,
            Color.CYAN,
            Color.GRAY,
            Color.GREEN,
            Color.MAGENTA,
            Color.RED,
            Color.YELLOW
    };

    // Game state
    private HashMap<IntPoint2D, Bubble> grid;
    ArrayList< ArrayList<IntPoint2D> > xyGrid;

    private float mCurX;
    private float mCurY;

    private enum GameEvent {
        TOUCH_DOWN,
        TOUCH_MOVE,
        TOUCH_UP,
        REDRAW
    }

    private enum GameState {
        GAME_WAITING,
        SHOOTER_AIMING,
        BUBBLE_MOVING,
        BUBBLES_EXPLODING
    }
    private GameState  mGameState;
    private IntPoint2D mAimingPoint;
    private Bubble     mMovingBubble;
    private double minDistanceToEmptyPoint = Double.POSITIVE_INFINITY;
    private IntPoint2D nearestEmptyPoint = null;


    public BubbleShooterView(Context context) {
        super(context);
        // Notify the SurfaceHolder that you'd like to receive SurfaceHolder callbacks.
        getHolder().addCallback(this);
        bst  = null;
        rng  = null;
        grid = null;
        mGameState    = GameState.GAME_WAITING;
        mAimingPoint  = null;
        mMovingBubble = null;
        minDistanceToEmptyPoint = Double.POSITIVE_INFINITY;
        nearestEmptyPoint = null;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // developer.android.com/guide/topics/graphics/2d-graphics.html
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Drawing to a Canvas is better when your application needs to regularly re-draw itself.
    // Applications such as video games should be drawing to the Canvas on its own.
    // Via the Canvas, your drawing is actually performed upon an underlying Bitmap,
    // which is placed into the window.
    // In the event that you're drawing within the onDraw() callback method, the Canvas is provided
    // for you and you need only place your drawing calls upon it. You can also acquire a Canvas
    // from SurfaceHolder.lockCanvas(), when dealing with a SurfaceView object.
    // if you need to create a new Canvas, then you must define the Bitmap upon which drawing will
    // actually be performed. The Bitmap is always required for a Canvas.
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        restartGame();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // The cleanest way to stop a thread is by interrupting it.
        // The thread must regularly check its interrupt flag.
        bst.interrupt();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        mCurX = e.getX();
        mCurY = e.getY();

        switch ( e.getAction() ) {
            case MotionEvent.ACTION_DOWN:
                updateGameState( GameEvent.TOUCH_DOWN );
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                updateGameState( GameEvent.TOUCH_UP );
                break;
        }
        return true;
    }

    protected void renderGame(Canvas c) throws InterruptedException {
//      throw new InterruptedException();

        updateGameState( GameEvent.REDRAW );

        drawAimingArrow(c);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        c.drawPaint(paint);
        for (Bubble bubble : grid.values()) {
            if (bubble == null) continue;
            paint.setColor(bubble.getColor());
            paint.setAlpha(bubble.alpha);
            c.drawCircle(bubble.getX(), bubble.getY(), bubble.getR(), paint);
        }
        if ( mMovingBubble != null ) {
            paint.setColor(mMovingBubble.getColor());
            c.drawCircle(mMovingBubble.getX(), mMovingBubble.getY(), mMovingBubble.getR(), paint);
        }
    }

    private void updateGameState( GameEvent event ) {
        switch( event ) {
            case TOUCH_DOWN:
                if ( mGameState == GameState.GAME_WAITING ) {
                    mGameState = GameState.SHOOTER_AIMING;
                }
                break;
            case TOUCH_MOVE:
                break;
            case TOUCH_UP:
                if ( mGameState == GameState.SHOOTER_AIMING ) {
                    mMovingBubble = grid.get( mAimingPoint );
                    int xVel = (int)mCurX - mAimingPoint.getX();
                    int yVel = (int)mCurY - mAimingPoint.getY();
                    double mag = Math.sqrt( xVel*xVel + yVel*yVel );
                    xVel = (int)( VELOCITY_SCALE * ( (double)xVel/mag ) );
                    yVel = (int)( VELOCITY_SCALE * ( (double)yVel/mag ) );
                    mMovingBubble.setVelocity( xVel, yVel );
                    grid.put( mAimingPoint, null );
                    mGameState = GameState.BUBBLE_MOVING;
                }
                break;
            case REDRAW: {
                int w = getWidth();
                int h = getHeight();
                switch( mGameState ) {
                    case GAME_WAITING:
                        break;
                    case SHOOTER_AIMING:
                        break;
                    case BUBBLE_MOVING:
                        assert( mMovingBubble != null );
                        if ( aboutToCollide() ) {
                            if ( nearestEmptyPoint == null ) {
                                displayMessage( "Game Over!", "Cannot move bubble anymore." );
                                restartGame();
                            } else {
                                mMovingBubble.setLocation(
                                        nearestEmptyPoint.getX(), nearestEmptyPoint.getY() );
                                grid.put( nearestEmptyPoint, mMovingBubble );
                                mMovingBubble = null;
                                removeSameColorBubbles( nearestEmptyPoint );
                                removeFloatingBubbles();
                                mGameState = GameState.BUBBLES_EXPLODING;
                            }
                        } else {
                            mMovingBubble.stepCoordinates( w, h );
                        }
                        break;
                    case BUBBLES_EXPLODING: {
                        boolean allBubblesExploded = true;
                        ArrayList<IntPoint2D> points = new ArrayList<IntPoint2D>( grid.keySet() );
                        for( IntPoint2D p : points ) {
                            Bubble bubble = grid.get(p);
                            if ( bubble == null) continue;
                            if ( ! bubble.fading ) continue;
                            if ( bubble.alpha > 0 ) {
                                allBubblesExploded = false;
                                bubble.alpha -= DELTA_ALPHA;
                                if ( bubble.alpha < 0 ) {
                                    bubble.alpha = 0;
                                }
                            } else {
                                grid.put( p, null );
                            }
                        }
                        if ( allBubblesExploded ) {
                            createCurBubble();
                            mGameState = GameState.GAME_WAITING;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    private void restartGame() {
        bst  = null;
        rng  = null;
        grid = null;
        mGameState    = GameState.GAME_WAITING;
        mAimingPoint  = null;
        mMovingBubble = null;
        minDistanceToEmptyPoint = Double.POSITIVE_INFINITY;
        nearestEmptyPoint = null;

        rng  = new Random();
        this.createGrid();
        this.createInitBubbles();
        createCurBubble();
        bst = new BubbleShooterThread(this);
        bst.start();
    }

    protected void drawAimingArrow(Canvas c) {
        if( mGameState != GameState.SHOOTER_AIMING ) return;

        int w = getWidth();
        int h = getHeight();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(2);
        paint.setColor(Color.BLACK);
        paint.setAlpha(128);
        paint.setAntiAlias(true);
        c.drawPaint(paint);
        c.drawLine(mAimingPoint.getX(), mAimingPoint.getY(), mCurX, mCurY, paint);
        paint.setStrokeWidth(0);
        c.drawLine(0, mCurY, w, mCurY, paint);
        c.drawLine(mCurX, 0, mCurX, h, paint);
    }

    private void createCurBubble() {
        assert( mAimingPoint != null );
        assert( grid.get( mAimingPoint ) == null );
        int color = bubbleColors[ rng.nextInt( bubbleColors.length ) ];
        grid.put( mAimingPoint, new Bubble( color, mAimingPoint.getX(), mAimingPoint.getY(), 0, 0 ) );
    }

    private void createInitBubbles() {
        int w = getWidth();
        int h = getHeight();
        ArrayList<IntPoint2D> points = new ArrayList<IntPoint2D>( grid.keySet() );
        for( IntPoint2D p : points ) {
            if ( p.getY() > (h/2) - Bubble.radius - Bubble.margin ) {
                continue;
            }
            int color = bubbleColors[ rng.nextInt( bubbleColors.length ) ];
            grid.put( p, new Bubble( color, p.getX(), p.getY(), 0, 0 ) );
        }
    }

    private boolean aboutToCollide() {
        assert( mMovingBubble != null );
        int w = getWidth();
        int h = getHeight();
        int x = mMovingBubble.getNextX(0, w);
        int y = mMovingBubble.getNextY(0, h);

        minDistanceToEmptyPoint = Double.POSITIVE_INFINITY;
        nearestEmptyPoint = null;
        boolean collides = false;

        ArrayList<IntPoint2D> points = new ArrayList<IntPoint2D>( grid.keySet() );
        for( IntPoint2D p : points ) {
            Bubble bub = grid.get(p);
            if ( bub == null ) continue;
            double dist = p.distance( x, y );
            if ( dist >= ( bub.getR() + mMovingBubble.getR() + Bubble.margin ) ) // No collision
                continue;
            collides = true;
            // Find the nearest empty grid point.
            // TODO: Too much redundant computations.
            isNearestEmptyPoint( p.leftNeighbor    , x, y );
            isNearestEmptyPoint( p.rightNeighbor   , x, y );
            isNearestEmptyPoint( p.upLeftNeighbor  , x, y );
            isNearestEmptyPoint( p.upRightNeighbor , x, y );
            isNearestEmptyPoint( p.lowLeftNeighbor , x, y );
            isNearestEmptyPoint( p.lowRightNeighbor, x, y );
        }
        return collides;
    }

    private void isNearestEmptyPoint( IntPoint2D p, int x, int y ) {
        assert( mMovingBubble != null );
        if ( p == null ) return;
        if ( grid.get( p ) != null ) return;
        double dist = p.distance( x, y );
        if ( (nearestEmptyPoint==null) || dist < minDistanceToEmptyPoint ) {
            minDistanceToEmptyPoint = dist;
            nearestEmptyPoint = p;
        }
    }

    private void createGrid() {
        grid = new HashMap<IntPoint2D, Bubble>();

        int w = getWidth();
        int h = getHeight();

        int midX = w / 2;
        int midY = h;
        double minDistance = Double.POSITIVE_INFINITY;

        xyGrid = new ArrayList< ArrayList<IntPoint2D> >();
        xyGrid.add( new ArrayList<IntPoint2D>() );
        int row = 0;

        int x = Bubble.radius + Bubble.margin;
        int y = Bubble.radius + Bubble.margin;
        boolean justify = true;
        int prevCol = -1;

        while( true ) {
            if ( ( x + Bubble.radius + Bubble.margin ) > w ) {
                y += 2 * Bubble.radius;
                x  =     Bubble.radius + Bubble.margin;
                if ( !justify ) {
                    justify = true;
                } else {
                    x += Bubble.radius + Bubble.margin;
                    justify = false;
                }
                xyGrid.add( new ArrayList<IntPoint2D>() );
                ++row;
            }
            if ( ( y + Bubble.radius ) > h ) {
                break;
            }
            int col = xyGrid.get(row).size();
            IntPoint2D p = new IntPoint2D(x, y, row, col);
            grid.put( p, null );

            IntPoint2D upLeft  = null;
            IntPoint2D upRight = null;
            IntPoint2D left    = null;
            if ( col > 0 ) { left = xyGrid.get(row).get(col-1); }
            if ( row==0 ) {
                upLeft  = null;
                upRight = null;
            } else if ( col==0 ) {
                boolean thisRowIsJustified = !justify;
                if ( !thisRowIsJustified ) {
                    upLeft  = null;
                    upRight = xyGrid.get(row-1).get(0);
                    prevCol = 0;
                } else {
                    upLeft  = xyGrid.get(row-1).get(0);
                    upRight = xyGrid.get(row-1).get(1);
                    prevCol = 1;
                }
            } else {
                assert( prevCol < xyGrid.get(row-1).size() );
                upLeft = xyGrid.get(row-1).get(prevCol);
                if ( (prevCol+1) < xyGrid.get(row-1).size() ) {
                    upRight = xyGrid.get(row-1).get(prevCol + 1);
                }
                ++prevCol;
            }
            p.setLeft      ( left    );
            p.setUpperLeft ( upLeft  );
            p.setUpperRight( upRight );
            xyGrid.get(row).add(p);

            x += 2 * Bubble.radius + Bubble.margin;
            double dist = p.distance(midX, midY);
            if ( ( mAimingPoint == null ) || ( dist < minDistance ) ) {
                minDistance = dist;
                mAimingPoint = p;
            }
        }
    }

    private void displayMessage( String title, String msg ) {
        new AlertDialog.Builder( this.getContext() )
                .setTitle( title )
                .setMessage( msg )
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
//              .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                  public void onClick(DialogInterface dialog, int which) {
//                      // do nothing
//                  }
//              })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void unmarkAll() {
        for( IntPoint2D point : grid.keySet() ) {
            point.marked = false;
        }
    }

    private void removeFloatingBubbles() {
        assert ( xyGrid.size() > 0 );
        unmarkAll();
        for( IntPoint2D p : xyGrid.get(0) ) {
            Bubble bubble = grid.get(p);
            if ( bubble == null ) continue;
            if ( bubble.fading  ) continue;
            markReachableBubbles( p );
        }
        for( IntPoint2D p : grid.keySet() ) {
            Bubble bubble = grid.get(p);
            if ( bubble == null ) continue;
            if ( p.marked ) continue;
            bubble.fading = true;
        }
    }

    private void markReachableBubbles( IntPoint2D p ) {
        // A grid point must be visited at most once.
        if ( p == null ) return;
        if ( p.marked  ) return;
        p.marked = true;

        Bubble bubble = grid.get(p);
        if ( bubble == null ) return;
        if ( bubble.fading  ) return;
        markReachableBubbles( p.leftNeighbor     );
        markReachableBubbles( p.rightNeighbor    );
        markReachableBubbles( p.upLeftNeighbor   );
        markReachableBubbles( p.upRightNeighbor  );
        markReachableBubbles( p.lowLeftNeighbor  );
        markReachableBubbles( p.lowRightNeighbor );
    }

    private void removeSameColorBubbles( IntPoint2D p ) {
        unmarkAll();
        int count = removeSameColorBubbles( p, 0, false );
        if ( count > 2 ) {
            unmarkAll();
            removeSameColorBubbles( p, 0, true );
        }
    }

    private int removeSameColorBubbles( IntPoint2D p, int count, boolean fadeOrCount ) {
        // A grid point must be visited at most once.
        if ( p.marked ) return count;
        p.marked = true;

        Bubble bubble = grid.get(p);
        assert( bubble != null );
        if ( fadeOrCount ) {
            bubble.fading = true;
        } else {
            ++count;
        }

        count = checkNeighborColor(p.leftNeighbor,     bubble, count, fadeOrCount);
        count = checkNeighborColor(p.rightNeighbor,    bubble, count, fadeOrCount);
        count = checkNeighborColor(p.upLeftNeighbor,   bubble, count, fadeOrCount);
        count = checkNeighborColor(p.upRightNeighbor,  bubble, count, fadeOrCount);
        count = checkNeighborColor(p.lowLeftNeighbor,  bubble, count, fadeOrCount);
        count = checkNeighborColor(p.lowRightNeighbor, bubble, count, fadeOrCount);
        return count;
    }

    private int checkNeighborColor( IntPoint2D neighbor, Bubble bubble, int count, boolean fadeOrCount ) {
        if( neighbor == null ) return count;
        Bubble neighborBubble = grid.get( neighbor );
        if ( neighborBubble == null ) return count;
        if ( neighborBubble.fading  ) return count;
        if ( neighborBubble.getColor() != bubble.getColor() ) return count;

        return removeSameColorBubbles( neighbor, count, fadeOrCount );
    }

}

