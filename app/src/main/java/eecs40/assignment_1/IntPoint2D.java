package eecs40.assignment_1;

/**
 * Created by Ahmed on 3/25/2015.
 */
public class IntPoint2D {

    private final int x; // Immutable to be usable as hash key
    private final int y; // Immutable to be usable as hash key

    private final int row; // Immutable to be usable as hash key
    private final int col; // Immutable to be usable as hash key

    public boolean    marked;
    public IntPoint2D leftNeighbor;
    public IntPoint2D rightNeighbor;
    public IntPoint2D lowLeftNeighbor;
    public IntPoint2D lowRightNeighbor;
    public IntPoint2D upLeftNeighbor;
    public IntPoint2D upRightNeighbor;

    public IntPoint2D( int x, int y, int row, int col ) {
        this.x   = x;
        this.y   = y;
        this.row = row;
        this.col = col;
        this.marked = false;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    public int getRow() { return row; }
    public int getCol() { return col; }

    public double distance( int px, int py ) {
        int dx = x - px;
        int dy = y - py;
        return Math.sqrt( dx * dx + dy * dy );
    }

    public double distance( IntPoint2D p ) {
        return distance( p.x, p.y );
    }

    public void setUpperLeft( IntPoint2D ul ) {
        if (ul == null) return;
        this.upLeftNeighbor   = ul;
        ul  .lowRightNeighbor = this;
    }

    public void setUpperRight( IntPoint2D ur ) {
        if (ur == null) return;
        this.upRightNeighbor = ur;
        ur  .lowLeftNeighbor = this;
    }

    public void setLeft( IntPoint2D left ) {
        if (left == null) return;
        this.leftNeighbor  = left;
        left.rightNeighbor = this;
    }

    @Override
    public int hashCode() {
        return( x + ( y + ( row  + col * 31 ) * 31 ) * 31 );
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof IntPoint2D) {
            IntPoint2D p = (IntPoint2D)obj;
            return x == p.x && y == p.y && row==p.row && col==p.col;
        }
        return false;
    }
}
