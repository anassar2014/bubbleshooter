package eecs40.assignment_1;

/**
 * Created by Ahmed on 3/24/2015.
 */
public class Bubble {
    public static final int radius   = 50;
    public static final int margin   = 5;
    private final int color;
    private final int r;
    private int deltaX;
    private int deltaY;
    private int x;
    private int y;
    private boolean reverseX;
    private boolean reverseY;
    private boolean collision;

    public int     alpha;
    public boolean fading;

    public Bubble(int color, int ix, int iy, int xDelta, int yDelta) {
        this(color, radius, ix, iy, xDelta, yDelta);
    }

    public Bubble(int color, int r, int ix, int iy, int deltaX, int deltaY) {
        this.color = color;
        this.r = r;
        this.x = ix;
        this.y = iy;
        this.reverseX = false;
        this.reverseY = false;
        this.deltaX   = deltaX;
        this.deltaY   = deltaY;
        alpha     = 255;
        fading    = false;
        collision = false;
    }

    public int getDeltaX() { return deltaX; }
    public int getDeltaY() { return deltaY; }
    public int getX() { return x; }
    public int getY() { return y; }

    public void setLocation( int x, int y ) {
        this.reverseX = false;
        this.reverseY = false;
        this.x = x;
        this.y = y;
    }

    public int getR() { return r; }
    public int getColor() { return color; }
    public void setVelocity( int deltaX, int deltaY ) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public void stepCoordinates( int maxX, int maxY ) {
        stepCoordinates( 0, maxX, 0, maxY );
    }

    public void stepCoordinates( int minX, int maxX, int minY, int maxY ) {
        x = getNextX( minX, maxX );
        reverseX = (collision)? !reverseX : reverseX;
        y = getNextY( minY, maxY );
        reverseY = (collision)? !reverseY : reverseY;
    }

    public int getNextX( int minX, int maxX ) {
        return getNextValue( x, minX, maxX, deltaX, reverseX );
    }

    public int getNextY( int minY, int maxY ) {
        return getNextValue( y, minY, maxY, deltaY, reverseY );
    }

    private int getNextValue( int curValue, int minValue, int maxValue, int delta, boolean reverse ) {
        int nextValue = curValue + ( ( !reverse )? delta : -delta );
        if ( ( nextValue + r + margin) > maxValue ) {
            collision = true;
            nextValue = maxValue - r - margin;
        } else if ( (nextValue - r - margin) < minValue ) {
            collision = true;
            nextValue = minValue + r + margin;
        } else {
            collision = false;
        }
        return nextValue;
    }
}
